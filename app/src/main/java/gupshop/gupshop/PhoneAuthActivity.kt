package gupshop.gupshop

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity

import android.text.TextUtils
//import jdk.nashorn.internal.runtime.ECMAException.getException
//import android.support.test.orchestrator.junit.BundleJUnitUtils.getResult
//import org.junit.experimental.results.ResultMatchers.isSuccessful
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.view.ViewGroup
import android.widget.Button
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.google.firebase.database.DatabaseException
import com.google.firebase.database.FirebaseDatabase
import gupshop.gupshop.model.User
import java.util.concurrent.TimeUnit


class PhoneAuthActivity : AppCompatActivity(),View.OnClickListener {

    private val TAG = "PhoneAuthActivity"

    private val KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress"

    private val STATE_INITIALIZED = 1
    private val STATE_CODE_SENT = 2
    private val STATE_VERIFY_FAILED = 3
    private val STATE_VERIFY_SUCCESS = 4
    private val STATE_SIGNIN_FAILED = 5
    private val STATE_SIGNIN_SUCCESS = 6
    private val STATE_SIGNED_IN = 7

    // [START declare_auth]
    private var mAuth: FirebaseAuth? = null
    // [END declare_auth]

    private var mVerificationInProgress = false
    private var mVerificationId: String? = null
    private var mResendToken: PhoneAuthProvider.ForceResendingToken? = null
    private var mCallbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks? = null

    private var mPhoneNumberViews: ViewGroup? = null
    private var mSignedInViews: ViewGroup? = null

    private var mStatusText: TextView? = null
    private var mDetailText: TextView? = null

    private var mPhoneNumberField: EditText? = null
    private var mVerificationField: EditText? = null

    private var mStartButton: Button? = null
    private var mVerifyButton: Button? = null
    private var mResendButton: Button? = null
    private var mSignOutButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_phone_auth)

        // Restore instance state
        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState)
        }

        // Assign views
        mPhoneNumberViews = findViewById(R.id.phone_auth_fields)
        mSignedInViews = findViewById(R.id.signed_in_buttons)

        mStatusText = findViewById(R.id.status)
        mDetailText = findViewById(R.id.detail)

        mPhoneNumberField = findViewById(R.id.field_phone_number)
        mVerificationField = findViewById(R.id.field_verification_code)

        mStartButton = findViewById(R.id.button_start_verification)
        mVerifyButton = findViewById(R.id.button_verify_phone)
        mResendButton = findViewById(R.id.button_resend)
        mSignOutButton = findViewById(R.id.sign_out_button)

        // Assign click listeners
        mStartButton!!.setOnClickListener(this)
        mVerifyButton!!.setOnClickListener(this)
        mResendButton!!.setOnClickListener(this)
        mSignOutButton!!.setOnClickListener(this)

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance()
        // [END initialize_auth]

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:$credential")
                // [START_EXCLUDE silent]
                mVerificationInProgress = false
                // [END_EXCLUDE]

                // [START_EXCLUDE silent]
                // Update the UI and attempt sign in with the phone credential
                updateUI(STATE_VERIFY_SUCCESS, credential)
                // [END_EXCLUDE]
                signInWithPhoneAuthCredential(credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e)
                // [START_EXCLUDE silent]
                mVerificationInProgress = false
                // [END_EXCLUDE]

                if (e is FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    mPhoneNumberField!!.error = "Invalid phone number."
                    // [END_EXCLUDE]
                } else if (e is FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    Snackbar.make(findViewById<View>(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show()
                    // [END_EXCLUDE]
                }

                // Show a message and update the UI
                // [START_EXCLUDE]
                updateUI(STATE_VERIFY_FAILED)
                // [END_EXCLUDE]
            }

            override fun onCodeSent(verificationId: String,
                           token: PhoneAuthProvider.ForceResendingToken) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:$verificationId")

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId
                mResendToken = token

                // [START_EXCLUDE]
                // Update UI
                updateUI(STATE_CODE_SENT)
                // [END_EXCLUDE]
            }
        }
        // [END phone_auth_callbacks]
    }

    // [START on_start_check_user]
    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = mAuth!!.getCurrentUser()
        updateUI(currentUser)

        // [START_EXCLUDE]
        if (mVerificationInProgress && validatePhoneNumber()) {
            startPhoneNumberVerification(mPhoneNumberField!!.text.toString())
        }
        // [END_EXCLUDE]
    }
    // [END on_start_check_user]

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS)
    }


    private fun startPhoneNumberVerification(phoneNumber: String) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber, // Phone number to verify
                60, // Timeout duration
                TimeUnit.SECONDS, // Unit of timeout
                this, // Activity (for callback binding)
                mCallbacks as PhoneAuthProvider.OnVerificationStateChangedCallbacks)        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true
    }

    private fun verifyPhoneNumberWithCode(verificationId: String, code: String) {
        // [START verify_with_code]
        val credential = PhoneAuthProvider.getCredential(verificationId, code)
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential)
    }

    // [START resend_verification]
    private fun resendVerificationCode(phoneNumber: String,
                                       token: PhoneAuthProvider.ForceResendingToken?) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber, // Phone number to verify
                60, // Timeout duration
                TimeUnit.SECONDS, // Unit of timeout
                this, // Activity (for callback binding)
                mCallbacks as PhoneAuthProvider.OnVerificationStateChangedCallbacks, // OnVerificationStateChangedCallbacks
                token)             // ForceResendingToken from callbacks
    }
    // [END resend_verification]

    // [START sign_in_with_phone]
    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(this, object : OnCompleteListener<AuthResult> {
                    override fun onComplete(task: Task<AuthResult>) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success")

                            val user = task.getResult().getUser()
                            if(task.getResult().additionalUserInfo.isNewUser){
                                try {
                                    val mDatabase = FirebaseDatabase.getInstance().getReference("users");
                                    val newUser = User(listOf("123"), listOf("abc"), "abc", "123", "abc@gmail.com", listOf("abc"), user.phoneNumber, user.uid);
                                    mDatabase.child(user.uid).setValue(newUser);
                                }catch( e : DatabaseException){
                                    Log.d(TAG, "Database exception "+e.message);
                                }
                            }
                                //TODO PRERNA
                                //start additional screen
                            // [START_EXCLUDE]
                            updateUI(STATE_SIGNIN_SUCCESS, user)
                            // [END_EXCLUDE]
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException())
                            if (task.getException() is FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                // [START_EXCLUDE silent]
                                mVerificationField!!.error = "Invalid code."
                                // [END_EXCLUDE]
                            }
                            // [START_EXCLUDE silent]
                            // Update UI
                            updateUI(STATE_SIGNIN_FAILED)
                            // [END_EXCLUDE]
                        }
                    }
                })
    }
    // [END sign_in_with_phone]

    private fun signOut() {
        mAuth!!.signOut()
        updateUI(STATE_INITIALIZED)
    }

    private fun updateUI(uiState: Int) {
        updateUI(uiState, mAuth!!.getCurrentUser(), null)
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            //prerna changes
            updateUI(STATE_SIGNED_IN, user)
        } else {
            updateUI(STATE_INITIALIZED)
        }
    }

    private fun updateUI(uiState: Int, user: FirebaseUser) {
        updateUI(uiState, user, null)
    }

    private fun updateUI(uiState: Int, cred: PhoneAuthCredential) {
        updateUI(uiState, null, cred)
    }

    private fun updateUI(uiState: Int, user: FirebaseUser?, cred: PhoneAuthCredential?) {
        when (uiState) {
            STATE_INITIALIZED -> {
                // Initialized state, show only the phone number field and start button
                enableViews(mStartButton as View, mPhoneNumberField as View)
                disableViews(mVerifyButton as View, mResendButton as View, mVerificationField as View)
                mDetailText!!.text = null
            }
            STATE_CODE_SENT -> {
                // Code sent state, show the verification field, the
                enableViews(mVerifyButton as View, mResendButton as View, mPhoneNumberField as View, mVerificationField as View)
                disableViews(mStartButton!!)
                mDetailText!!.setText(R.string.status_code_sent)
            }
            STATE_VERIFY_FAILED -> {
                // Verification has failed, show all options
                enableViews(mStartButton as View, mVerifyButton as View, mResendButton as View, mPhoneNumberField as View,
                        mVerificationField as View)
                mDetailText!!.setText(R.string.status_verification_failed)
            }
            STATE_VERIFY_SUCCESS -> {
                // Verification has succeeded, proceed to firebase sign in
                disableViews(mStartButton as View, mVerifyButton as View, mResendButton as View, mPhoneNumberField as View,
                        mVerificationField as View)
                mDetailText!!.setText(R.string.status_verification_succeeded)

                // Set the verification text based on the credential
                if (cred != null) {
                    if (cred!!.getSmsCode() != null) {
                        mVerificationField!!.setText(cred!!.getSmsCode())
                    } else {
                        mVerificationField!!.setText(R.string.instant_validation)
                    }
                }
            }
            STATE_SIGNIN_FAILED ->
                // No-op, handled by sign-in check
                mDetailText!!.setText(R.string.status_sign_in_failed)
            STATE_SIGNIN_SUCCESS -> {
                var intent = Intent(this,HomePage::class.java)
                startActivity(intent)
            }
        }// Np-op, handled by sign-in check

        if (user == null) {
            // Signed out
            mPhoneNumberViews!!.visibility = View.VISIBLE
            mSignedInViews!!.visibility = View.GONE

            mStatusText!!.setText(R.string.signed_out)
        } else if(uiState != STATE_SIGNIN_SUCCESS){
            // Signed in
            mPhoneNumberViews!!.visibility = View.GONE
            mSignedInViews!!.visibility = View.VISIBLE

            enableViews(mPhoneNumberField as View, mVerificationField as View)
            mPhoneNumberField!!.setText(null)
            mVerificationField!!.setText(null)

            mStatusText!!.setText(R.string.signed_in)
            mDetailText!!.text = getString(R.string.firebase_status_fmt, user!!.getUid())
        }
    }

    private fun validatePhoneNumber(): Boolean {
        val phoneNumber = mPhoneNumberField!!.text.toString()
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField!!.error = "Invalid phone number."
            return false
        }

        return true
    }

    private fun enableViews(vararg views: View) {
        for (v in views) {
            v.setEnabled(true)
        }
    }

    private fun disableViews(vararg views: View) {
        for (v in views) {
            v.setEnabled(false)
        }
    }

    override fun onClick(view: View) {
        when (view.getId()) {
            R.id.button_start_verification -> {
                if (!validatePhoneNumber()) {
                    return
                }

                startPhoneNumberVerification(mPhoneNumberField!!.text.toString())
            }
            R.id.button_verify_phone -> {
                val code = mVerificationField!!.text.toString()
                if (TextUtils.isEmpty(code)) {
                    mVerificationField!!.error = "Cannot be empty."
                    return
                }

                verifyPhoneNumberWithCode(mVerificationId as String, code)
            }
            R.id.button_resend -> resendVerificationCode(mPhoneNumberField!!.text.toString(), mResendToken)
            R.id.sign_out_button -> signOut()
        }
    }

}
