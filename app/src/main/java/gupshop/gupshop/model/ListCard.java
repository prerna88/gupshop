package gupshop.gupshop.model;

public class ListCard {
    private String mMarker;
    private String mCategory;

    public ListCard(String marker,String category) {
        this.mMarker = marker;
        this.mCategory = category;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getMarker(){
        return mMarker;
    }

}

