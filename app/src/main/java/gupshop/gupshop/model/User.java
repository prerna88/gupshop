package gupshop.gupshop.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;

@IgnoreExtraProperties
public class User {
    List<String> mActiveConversation;
    List<String> mAddresses;
    String mCurrentAddress;
    String mDeviceID;
    String mEmail;
    List<String> mFavShops;
    String mPhoneNumber;
    String mUUID;

    public User(){

    }

    public User(List<String> activeConversation, List<String> addresses, String currentAddress, String deviceID, String eMail, List<String> favShops, String phoneNumber, String uuid)
    {
        this.mActiveConversation = activeConversation;
        this.mAddresses = addresses;
        this.mCurrentAddress = currentAddress;
        this.mDeviceID = deviceID;
        this.mEmail = eMail;
        this.mFavShops = favShops;
        this.mPhoneNumber = phoneNumber;
        this.mUUID = uuid;
    }

}
