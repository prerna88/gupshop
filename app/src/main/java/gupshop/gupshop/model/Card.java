package gupshop.gupshop.model;

public class Card
{
    private String mCategory;

    public Card(String category) {
        this.mCategory = category;
    }

    public String getCategory() {
        return mCategory;
    }

}
