package gupshop.gupshop

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ListView
import android.widget.TextView
import com.google.firebase.database.FirebaseDatabase
import gupshop.gupshop.Adapter.ListPageCardArrayAdapter
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener
import gupshop.gupshop.model.ListCard


class ListPage : AppCompatActivity() {

    private var mListView: ListView? = null
    private var mListPageCardArrayAdapter : ListPageCardArrayAdapter? = null
    private var mTitle: TextView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_page)
        val category:String = intent.getStringExtra("category")

        mListView = findViewById(R.id.card_listView_listpage)
        mTitle =findViewById(R.id.title_listPage)
        mTitle!!.setText("Shops : "+category)

        mListPageCardArrayAdapter = ListPageCardArrayAdapter(applicationContext, R.layout.list_item_card)
        val mDatabase = FirebaseDatabase.getInstance().getReference("shop")
        mDatabase.addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        //Get map of users in datasnapshot
                        collectShopNames(dataSnapshot.value as Map<Any, Any>? , category)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        //handle databaseError
                    }
                })

        mListView!!.setAdapter(mListPageCardArrayAdapter);
        mListView!!.onItemClickListener = AdapterView.OnItemClickListener{ adapterView, view, position, id ->
           // val intent = Intent(this,ListPage::class.java)
            //intent.putExtra("category", mCardArrayAdapter!!.getItem(position).category)
            //startActivity(intent)
        }



    }

    private fun collectShopNames(map: Map<Any,Any>? , category:String)
    {
        for (entry in map!!.entries) {

            //Get user map
            System.out.println("prerna "+entry.value)
            val singleUser = entry.value as Map<*, *>
            if((singleUser.get("category") as java.lang.String)!!.equalsIgnoreCase(category))
            {
                var name = singleUser.get("name")
                mListPageCardArrayAdapter!!.add(ListCard(name.toString().substring(0,1), name.toString()))
            }
        }

    }
}
