package gupshop.gupshop

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import gupshop.gupshop.Adapter.CardArrayAdapter
import gupshop.gupshop.model.Card
import android.widget.AdapterView.OnItemClickListener



class HomePage : AppCompatActivity() {

    private var mListView: ListView? = null
    private final val TAG = "HomePageActivity"
    private var mCardArrayAdapter : CardArrayAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_page)
        mListView = findViewById(R.id.card_listView)

        mCardArrayAdapter = CardArrayAdapter(applicationContext, R.layout.list_item_card_home)
        mCardArrayAdapter!!.add(Card("Grocery"))
        mCardArrayAdapter!!.add(Card("Food"))
        mCardArrayAdapter!!.add(Card("Veggies"))

        mListView!!.setAdapter(mCardArrayAdapter);
        mListView!!.onItemClickListener = AdapterView.OnItemClickListener{ adapterView, view, position, id ->
           val intent = Intent(this,ListPage::class.java)
            intent.putExtra("category", mCardArrayAdapter!!.getItem(position).category)
            startActivity(intent)
        }
    }
}
