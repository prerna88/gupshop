package gupshop.gupshop.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gupshop.gupshop.R;
import gupshop.gupshop.model.Card;
import gupshop.gupshop.model.ListCard;

public class ListPageCardArrayAdapter extends ArrayAdapter<ListCard> {
    private static final String TAG = "ListPageCardArrayAdapter";
    private List<ListCard> cardList = new ArrayList<ListCard>();

    static class ListCardViewHolder {
        TextView marker;
        TextView category;
    }

    public ListPageCardArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public void add(ListCard object) {
        cardList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.cardList.size();
    }

    @Override
    public ListCard getItem(int index) {
        return this.cardList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ListPageCardArrayAdapter.ListCardViewHolder viewHolder;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.list_item_card, parent, false);
            viewHolder = new ListPageCardArrayAdapter.ListCardViewHolder();
            viewHolder.marker = (TextView) row.findViewById((R.id.marker));
            viewHolder.category = (TextView) row.findViewById(R.id.shop_name);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ListPageCardArrayAdapter.ListCardViewHolder)row.getTag();
        }
        ListCard card = getItem(position);
        viewHolder.category.setText(card.getCategory());
        viewHolder.marker.setText(card.getMarker());
        return row;

    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

}

